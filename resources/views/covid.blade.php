<h1>{{ $data->countryregion}}</h1>
<ul>
    <li>Casos Confirmados : <b><span class="count">{{$data->confirmed}}</span></b></li>
    <li>Fallecidos : <b><span class="count">{{$data->deaths}}</span></b></li>
    <li>Recuperados : <b><span class="count">{{$data->recovered}}</span></b></li>
</ul>