<p>
<label>Select Country:</label>
<select name="country">
	<option value="">All Countries</option>
	@foreach ( $countries as $iso => $country )
		<option value="{{$iso}}"
		@if($instance['country'] == $iso)
		selected
		@endif
		>{{$country}}</option>
	@endforeach
</select>
</p>
<p>
<label>Show mundial data too?</label>
<input type="checkbox" name="all" value="{{$instance['all']}}">
</p>