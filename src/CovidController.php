<?php
    namespace ANTOCOVID;
    
          
    class CovidController
    {
    
        public function __construct()
        {
    
        }

        public static function COVIDdata( $atts, $content = null)
        {
            $a = shortcode_atts( array(
                'country' => 'country',
                ), $atts );
            $c = curl_init("https://wuhan-coronavirus-api.laeyoung.endpoint.ainize.ai/jhu-edu/latest?iso2={$atts['country']}&onlyCountries=true");
            curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
            $html = curl_exec($c);
            if (curl_error($c)){
                die(curl_error($c));
            }
            $status = curl_getinfo($c, CURLINFO_HTTP_CODE);
            curl_close($c);
            return view('covid',['data' => json_decode($html)[0]]);
            //[{"countryregion":"Peru","lastupdate":"2020-04-28T20:42:00.002Z","location":{"lat":-9.19,"lng":-75.0152},"countrycode":{"iso2":"PE","iso3":"PER"},"confirmed":28699,"deaths":782,"recovered":8425}]
        }
    }